// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyPlayerController.h"
#include "GameFramework/Character.h"
#include "PaperFlipbookComponent.h"
#include "Windows/AllowWindowsPlatformTypes.h"
#include "MyCharacter.generated.h"


UCLASS()
class ALUMINUM_SLUG_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	
	// Sets default values for this character's properties
	AMyCharacter();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Paper)
	UPaperFlipbookComponent* legs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Paper)
	UPaperFlipbookComponent* top;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
