// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"

#include "Bullet.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PaperCharacter.h"
#include "PaperFlipbookComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GenericPlatform/GenericPlatformCrashContext.h"

AMyPlayerController::AMyPlayerController() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMyPlayerController::BeginPlay() {
	Super::BeginPlay();
	myCharacter = Cast<APaperCharacter>(GetCharacter());
}

void AMyPlayerController::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (inputPressedRight) {
		myCharacter->AddMovementInput(FVector(0, 10, 0), 2.0);
	} else if (inputPressedLeft) {
		myCharacter->AddMovementInput(FVector(0, -10, 0), 2.0);
	}

	if (inputPressedJump) {
		myCharacter->Jump();
	} else { }

	if (inputPressedAimUp) { }
	
}

void AMyPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	UE_LOG(LogTemp, Warning, TEXT("Cock&Balls"));

	InputComponent->BindAction("MoveRightCharacter", IE_Pressed, this, &AMyPlayerController::OnRightMovementPressed);
	InputComponent->BindAction("MoveRightCharacter", IE_Released, this, &AMyPlayerController::OnRightMovementReleased);

	InputComponent->BindAction("MoveLeftCharacter", IE_Pressed, this, &AMyPlayerController::OnLeftMovementPressed);
	InputComponent->BindAction("MoveLeftCharacter", IE_Released, this, &AMyPlayerController::OnLeftMovementReleased);

	InputComponent->BindAction("JumpCharacter", IE_Pressed, this, &AMyPlayerController::OnJumpPressed);
	InputComponent->BindAction("JumpCharacter", IE_Released, this, &AMyPlayerController::OnJumpReleased);

	InputComponent->BindAction("AimUpCharacter", IE_Pressed, this, &AMyPlayerController::OnAimUpPressed);
	InputComponent->BindAction("AimUpCharacter", IE_Released, this, &AMyPlayerController::OnAimUpReleased);

	InputComponent->BindAction("Shoot", IE_Pressed, this, &AMyPlayerController::OnFirePressed);
	InputComponent->BindAction("Interact", IE_Pressed, this, &AMyPlayerController::OnInteract);
}

void AMyPlayerController::OnRightMovementPressed() {
	inputPressedRight = true;
}

void AMyPlayerController::OnRightMovementReleased() {
	inputPressedRight = false;
}

void AMyPlayerController::OnLeftMovementPressed() {
	inputPressedLeft = true;
}

void AMyPlayerController::OnLeftMovementReleased() {
	inputPressedLeft = false;
}

void AMyPlayerController::OnJumpPressed() {
	inputPressedJump = true;
}

void AMyPlayerController::OnJumpReleased() {
	inputPressedJump = false;
}

void AMyPlayerController::OnAimUpPressed() {
	inputPressedAimUp = true;
}

void AMyPlayerController::OnAimUpReleased() {
	inputPressedAimUp = false;
}

void AMyPlayerController::OnInteract() {
	
}

void AMyPlayerController::OnFirePressed() {
	if(IsLocalController()) {
		ServerCallOnFire(myCharacter, mBulletBP);
	}
}

void AMyPlayerController::ServerCallOnFire_Implementation(APaperCharacter* amyCharacter, TSubclassOf<ABullet> amBulletBP) {
	if (IsValid(amBulletBP) && canShoot) {
		canShoot = false;
		GetWorld()->GetTimerManager().SetTimer(shootDelay, this,
			&AMyPlayerController::AuthSetShootCD, .25f, false);
		const FVector SpawnPosition = amyCharacter->GetActorLocation();
		const FRotator SpawnRotation = amyCharacter->GetActorRelativeScale3D().Rotation();
		auto* bala = GetWorld()->SpawnActor<ABullet>(amBulletBP, SpawnPosition, SpawnRotation);
		bala->SetOwner(this);
		if (inputPressedAimUp) {
			FVector velocitatInicial = amyCharacter->GetActorUpVector() * 1000.0f;
			bala->Velocity = velocitatInicial;
		} else {
			double velocitatInicial = amyCharacter->GetSprite()->GetRelativeScale3D().X * 1000.0;
			UE_LOG(LogTemp, Warning, TEXT("%s - %d"), *amyCharacter->GetName(), velocitatInicial);
			bala->Velocity = {velocitatInicial,0,0};
		}
	}
}

void AMyPlayerController::AuthSetShootCD() {
	canShoot = true;
}
