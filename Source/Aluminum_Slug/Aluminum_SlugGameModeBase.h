// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Aluminum_SlugGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ALUMINUM_SLUG_API AAluminum_SlugGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
