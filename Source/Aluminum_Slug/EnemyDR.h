// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "EnemyDR.generated.h"


//Data row de los enemigos

UENUM(BlueprintType)
enum FEEnemyType
{
	SOLDAT,
	SOLDATAVANCAT,
	CAPORAL,
	CAPORALAVANCAT,
	HELICOPTER,
	TANC,
	METALSLUG,
};

UENUM(BlueprintType)
enum FEMovementType
{
	ESTATIC,
	PATRULLA,
	AERI
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct ALUMINUM_SLUG_API FEnemyDR : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<FEEnemyType> Enemy;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int InitialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<FEMovementType> MovementType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ShootingRate;
};
