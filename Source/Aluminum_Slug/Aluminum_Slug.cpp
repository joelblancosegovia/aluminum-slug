// Copyright Epic Games, Inc. All Rights Reserved.

#include "Aluminum_Slug.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Aluminum_Slug, "Aluminum_Slug" );
