// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

UCLASS()
class ALUMINUM_SLUG_API ABullet : public APawn{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABullet();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Paper)
	USceneComponent* Scene;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Paper)
	class UPaperFlipbookComponent* bulletSprite;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Movement)
	class UFloatingPawnMovement* mMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Movement)
	FVector Velocity = FVector(1.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Damage)
	int32 Damage = 2;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;
};
