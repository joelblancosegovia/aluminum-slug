// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "PaperZD/Public/PaperZDCharacter.h"
#include "Aluminum_Slug/EnemyDR.h"
#include "TriggerActor.generated.h"

USTRUCT(BlueprintType)
struct FSpawnInfo {
	GENERATED_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Position)
	FVector posicioSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Position)
	TSubclassOf<APaperZDCharacter> tipusEnemic;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<FEEnemyType> Enemy;
	
};

UCLASS()
class ALUMINUM_SLUG_API ATriggerActor : public AActor {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATriggerActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Position)
	TArray<FSpawnInfo> enemiesSpawnInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
