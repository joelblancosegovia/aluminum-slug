// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"

#include "Bullet.h"
#include "PaperFlipbookComponent.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();


	
	legs = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("LegsComp"));
	legs->SetupAttachment(GetMesh());

	top = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("TopComp"));
	top->SetupAttachment(legs);
	
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AEnemyCharacter::ShootToPlayer() const {
	if(IsValid(mBulletBP)) {
		const FVector SpawnPosition = GetActorLocation();
		const FRotator SpawnRotation = GetActorRelativeScale3D().Rotation();
		auto bala = GetWorld()->SpawnActor<ABullet>(mBulletBP, SpawnPosition, SpawnRotation);
		FVector velocitatInicial = GetActorForwardVector() * 500.0f;
		bala->Velocity *= velocitatInicial;
	}
}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

