// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

#include "PaperFlipbookComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/FloatingPawnMovement.h"

// Sets default values
ABullet::ABullet()
{
	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Rut"));
	RootComponent = Scene;
	bulletSprite = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("spriteComp"));
	bulletSprite->SetupAttachment(Scene);
	mMovement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));
	bReplicates = true;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABullet::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
}



