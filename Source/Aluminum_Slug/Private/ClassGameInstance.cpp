// Fill out your copyright notice in the Description page of Project Settings.


#include "ClassGameInstance.h"
#include "AITestsCommon.h"
#include "OnlineSessionSettings.h"
#include "Kismet/GameplayStatics.h"

void UClassGameInstance::Init() {
	Super::Init();
	if(IOnlineSubsystem* sub = IOnlineSubsystem::Get()) {
		SessionInterface = sub->GetSessionInterface();
		if(SessionInterface.IsValid()) {
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UClassGameInstance::OnCreateSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UClassGameInstance::OnJoinSessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UClassGameInstance::OnFindSessionComplete);
		}
	}
}

void UClassGameInstance::OnCreateSessionComplete(FName SessionName, bool Succeded) {
	if(Succeded) {
		UE_LOG(LogTemp, Warning, TEXT("Creacionado"));
		FAITestHelpers::GetWorld()->ServerTravel("/Game/Missions/Mission1-1?listen");
	}
}
void UClassGameInstance::OnFindSessionComplete(bool Succeded) {
	if(Succeded) {
		UE_LOG(LogTemp, Warning, TEXT("sussis"));
		auto searchResult = SessionSearch->SearchResults;

		if(searchResult.Num()) {
			SessionInterface->JoinSession(0,FName("Pipo"),searchResult[0]);
		}
	}
}
void UClassGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) {
	UE_LOG(LogTemp, Warning, TEXT("jolin"));

	if(APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0)) {
		FString joinAddress{""};
		SessionInterface->GetResolvedConnectString(SessionName, joinAddress);
		if(joinAddress != "") {
			PlayerController->ClientTravel(joinAddress, ETravelType::TRAVEL_Absolute);
		}
	}
}

void UClassGameInstance::CreateServer() {
	UE_LOG(LogTemp, Warning, TEXT("Create server"));

	FOnlineSessionSettings s;
	s.bAllowJoinInProgress = true;
	s.bIsDedicated = false;
	s.bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	s.bShouldAdvertise=true;
	s.bUsesPresence=true;
	s.NumPublicConnections=4;

	SessionInterface->CreateSession(0, FName("Pipo"), s);
}
void UClassGameInstance::JoinServer() {
	UE_LOG(LogTemp, Warning, TEXT("Join server"));

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	SessionSearch->bIsLanQuery = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	SessionSearch->MaxSearchResults = 100;
	SessionSearch->QuerySettings.Set("SEARCH_PRESENCE", true, EOnlineComparisonOp::Equals);

	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
}
void UClassGameInstance::PlayAlone() {
	UE_LOG(LogTemp, Warning, TEXT("Juegacion solitario"));
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Missions/Mission1-1", true);
}