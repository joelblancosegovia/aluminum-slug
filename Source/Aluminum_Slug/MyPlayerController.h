// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ALUMINUM_SLUG_API AMyPlayerController : public APlayerController {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyPlayerController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ABullet> mBulletBP;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual void SetupInputComponent() override;

public:
	void OnRightMovementPressed();
	void OnRightMovementReleased();
	void OnLeftMovementPressed();
	void OnLeftMovementReleased();
	void OnJumpPressed();
	void OnJumpReleased();
	void OnAimUpPressed();
	void OnAimUpReleased();
	void OnInteract();
	void OnFirePressed();

	void AuthSetShootCD();
	
	UFUNCTION(Server, Reliable)
	void ServerCallOnFire(APaperCharacter* amyCharacter, TSubclassOf<ABullet> amBulletBP);

	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool inputPressedAimUp = false;
private:
	bool inputPressedRight = false;
	bool inputPressedLeft = false;
	bool inputPressedJump = false;
	APaperCharacter* myCharacter;
	FTimerHandle shootDelay {};

	bool canShoot{true};
	

};
